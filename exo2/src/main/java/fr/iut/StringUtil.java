package fr.iut;

import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by romjallet on 11/02/15.
 */
public class StringUtil {

    public String prettyCurrencyPrint(final double amount, final Locale locale) {
        NumberFormat nf = NumberFormat.getCurrencyInstance(locale);
        return nf.format(amount);
    }

}
