package fr.iut;
import org.junit.Assert;
import org.junit.Test;
import java.util.Locale;

public class StringUtilTest {
    @Test
    public void prettyCurrencyPrintTest() {
        StringUtil util = new StringUtil();
        Assert.assertEquals(util.prettyCurrencyPrint(21500.390, Locale.FRANCE), "21 500,39 €");
    }
}