<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>${title}</title>

    <!-- Bootstrap -->
    <link href="${root}css/bootstrap.min.css" rel="stylesheet">
    <link href="${root}css/style.css" rel="stylesheet">
</head>

    <body>
        <div id="content" class="error">
            <h1>${title}</h1>
            <p>${message}</p>
            <div class="bottom">
                <a href="admin/create">Retour page précédente</a>
            </div>
        </div>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="${root}js/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="${root}js/bootstrap.min.js"></script>
    </body>
</html>