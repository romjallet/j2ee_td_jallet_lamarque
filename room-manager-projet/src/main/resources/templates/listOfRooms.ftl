<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>${title}</title>

    <!-- Bootstrap -->
    <link href="${root}css/bootstrap.min.css" rel="stylesheet">
    <link href="${root}css/style.css" rel="stylesheet">
</head>

    <body>
    <div class="container">

        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">Room Manager</a>
                <ul class="nav navbar-nav">
                    <li ><a href="index.html">Accueil</a></li>
                    <li class="active"><a href="rooms">Liste des Salles</a></li>
                     <#if isAdmin?string('yes', 'no') == 'yes'>
                         <li><a href="rooms">Administration</a></li>
                         <li><a href="create">Ajouter une salle</a></li>
                            <#else>
                         <li><a href="admin/rooms">Administration</a></li>
                     </#if>

                </ul>
            </div>
            </div>
        </nav>
        <div id="content">
            <h1 style="text-align:center">${title}</h1>
            <ul>
                <#list rooms as room>
                <div class="jumbotron">
                    <li>
                        <#assign capacity = room.getCapacity()>
                        <#assign nbP = room.getNbPeopleInside()>
                        <#assign percent = ((nbP / capacity)*100)?int>

                        <#if percent < 40>
                            <#assign color="green">
                        <#elseif percent < 70>
                            <#assign color="orange">
                        <#else>
                            <#assign color="red">
                        </#if>

                        <h3><span class="glyphicon glyphicon-flag" aria-hidden="true" style="color: ${color};"></span> Salle ${room.getName()}</h3>
                        <p>${room.getDescription()}</p>
                        <p>${room.getNbPeopleInside()} / ${room.getCapacity()} <span style="color: ${color};">( ${percent}% )</span></p>

                        <div>
                            <#if room.getNbPeopleInside() < room.getCapacity()>
                                <a class="btn btn-primary" role="button" href="${root}inout?t=i&name=${room.getName()}&url=${url}">Entrer</a>
                            <#else>
                                <a class="btn btn-primary disabled" role="button" href="#">Entrer</a>
                            </#if>

                            <#if 0 < room.getNbPeopleInside()>
                                <a class="btn btn-success" role="button" href="${root}inout?t=o&name=${room.getName()}&url=${url}">Quitter</a>
                            <#else>
                                <a class="btn btn-success disabled" role="button" href="#">Quitter</a>
                            </#if>

                            <#if isAdmin?string('yes', 'no') == 'yes'>
                                <a class="btn btn-danger" role="button" href="remove?name=${room.getName()}">Supprimer</a>
                            </#if>
                        </div>

                    </li>
                    </div>
                </#list>
            </ul>

            </div>
        </div>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="${root}js/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="${root}js/bootstrap.min.js"></script>
    </body>
</html>