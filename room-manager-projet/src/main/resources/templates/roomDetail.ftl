<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>${title}</title>

    <!-- Bootstrap -->
    <link href="${root}css/bootstrap.min.css" rel="stylesheet">
    <link href="${root}css/style.css" rel="stylesheet">
</head>

    <body>
    <div class="container">

        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">Room Manager</a>
                <ul class="nav navbar-nav">
                    <li ><a href="index.html">Accueil</a></li>
                    <li class="active"><a href="rooms">Liste des Salles</a></li>
                    <#if isAdmin?string('yes', 'no') == 'yes'>
                       <li><a href="rooms">Administration</a></li>
                       <li><a href="create">Ajouter une salle</a></li>
                              <#else>
                       <li><a href="admin/rooms">Administration</a></li>
                    </#if>
                </ul>
            </div>
            </div>
        </nav>
        <div class="jumbotron">
        <!--<div id="content" class="info">
            <h1 style="text-align:center">${title}</h1>
            <h3 style="text-align:center">Nom de la salle</h3>
            <p> ${room.getName()}</p>
            <h3 style="text-align:center">Places occupees</h3>
            <p>${room.getNbPeopleInside()} / ${room.getCapacity()}</p>
            <h3 style="text-align:center">Description</h3>
            <p> ${room.getDescription()}</p>
        </div>-->
        <h1 style="text-align:center">${title}</h1>
        <div class="panel panel-default">
         <div id="content" class="info">
          <div class="panel-heading" style="text-align:center"><h2>${room.getName()}</h2></div>
          <div class="panel-body">
            Places occupees : ${room.getNbPeopleInside()} / ${room.getCapacity()} <br>
            Description : <br>
            ${room.getDescription()}
          </div>
          </div>
           </div>
        </div>
        </div>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="${root}js/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="${root}js/bootstrap.min.js"></script>
    </body>
</html>