<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>${title}</title>

    <!-- Bootstrap -->
    <link href="${root}css/bootstrap.min.css" rel="stylesheet">
    <link href="${root}css/style.css" rel="stylesheet">
</head>

<body>
<div id="content">
<div class="container">

    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">Room Manager</a>
            <ul class="nav navbar-nav">
                <li class="active"><a href="index.html">Accueil</a></li>
                <li><a href="rooms">Liste des Salles</a></li>
                <li><a href="rooms">Administration</a></li>
            </ul>
        </div>
        </div>
    </nav>
<!--    <h1>${title}</h1>
    <form method="get" action="init">
        <label for="name">Nom* : </label>
        <input id="name" name="name" type="text" maxlength="30" placeholder="Nom de la salle (entre 3 et 30 caractères)"/><br/>

        <label for="capacity">Capacité* :</label>
        <input id="capacity" name=capacity type="number" min="1" value="30"/><br/>

        <label for="description">Description :</label>
        <input id="description" name="description" type="text" maxlength="255" placeholder="Description de la salle (max 255 caractères)"/><br/><br/>

        <input type="submit" value="Créer"/><input type="reset" value="Annuler"/>
    </form>
</div>
</div>-->

<div class="container" class="z-depth-5" style="border">
            <div class="row">
                <form class="col s12" method="get" action="init">
                <h1 style="text-align:center">${title}</h1>
                    <div class="row">
                        <div class="input-field col s6">
                             <label for="name">Nom : </label>
                              <input id="name" name="name" type="text" maxlength="30" placeholder="Nom de la salle (entre 3 et 30 caractères)"/><br/>
                        </div>
                        <div class="input-field col s6">
                            <label for="capacity">Capacite :</label>
                            <input id="capacity" name=capacity type="number" min="1" value="30"/><br/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <label for="description">Description :</label>
                            <input id="description" name="description" type="text" maxlength="255" placeholder="Description de la salle (max 255 caractères)"/><br/><br/>
                        </div>
                    </div>
                    <div class="input-field col s6">
                        <input type="submit" value="Creer"/>
                            <i class="mdi-content-send right"></i>
                          <input type="reset" value="Annuler"/>
                    </div>
                </form>
            </div>

        </div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="${root}js/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="${root}js/bootstrap.min.js"></script>
</body>
</html>