package fr.iut.rm.persistence.domain;

import javax.persistence.*;

//import javax.validation.constraints.Size;

/**
 * A classic room
 */
@Entity
@Table(name = "room")
public class Room {
    /**
     * sequence generated id
     */
    @Id
    @GeneratedValue
    private long id;

    /**
     * Room's name
     */
    //@Size(min=3, max=30)
    @Column(nullable = false, unique = true, length = 30)
    private String name;
    /**
     * Room's description
     */
    //ça ne sert à rien de précisier nullable, unique, etc, leurs valeurs par défaut sont très bien (nottemment 255 de long)
    @Column
    private String description;
    /**
     * Room's capacity
     */
    @Column(nullable = false)
    private int capacity;
    /**
     * Room's insiders
     */
    @Column
    private int nbPeopleInside;
    /**
     * Default constructor (do nothing)
     */
    public Room() {
        // do nothing
    }

    /**
     * anemic getter
     *
     * @return the room's id
     */
    public long getId() {
        return id;
    }

    /**
     * anemic setter
     *
     * @param id the new id
     */
    public void setId(final long id) {
        this.id = id;
    }

    /**
     * anemic getter
     *
     * @return the calling number
     */
    public String getName() {
        return name;
    }

    /**
     * anemic setter
     *
     * @param name the new calling number to set
     * @throws IllegalArgumentException if the length of the name isn't between 3 and 30 characters
     */
    public void setName(final String name) throws IllegalArgumentException{
        if( name.length()<3 || 30<name.length() ){
            throw new IllegalArgumentException("Le nom de la salle doit contenir entre 3 et 30 caractères.");
        }
        this.name = name;
    }


    /**
     * anemic getter
     * @return the description of the room
     */
    public String getDescription() {
        return description;
    }

    /**
     * anemic setter
     *
     * @param description the new description of the room
     */
    public void setDescription(String description) {
        if(description.length()>255)
            this.description = description.substring(0,254);
        else
            this.description = description;
    }

    /**
     * anemic getter
     *
     * @return the capacity of the room
     */
    public int getCapacity() {
        return capacity;
    }

    /**
     * anemic setter
     *
     * @param capacity the new capacity of the room
     */
    public void setCapacity(int capacity) throws IllegalArgumentException {
        if(capacity<=0){
            throw new IllegalArgumentException("La capacité de la salle doit être un nombre strictement positif !");
        }
        this.capacity = capacity;
    }

    /**
     * anemic getter
     *
     * @return the number of people in the room
     */
    public int getNbPeopleInside() {
        return nbPeopleInside;
    }

    /**
     * anemic setter
     *
     * @param nbPeopleInside the new number of people inside the room
     */
    public void setNbPeopleInside(int nbPeopleInside) throws IllegalArgumentException{
        if(nbPeopleInside<0 || capacity<nbPeopleInside){
            throw new IllegalArgumentException("Le nombre de personnes dans la salle doit être compris entre 0 et la capacité max de la salle");
        }
        this.nbPeopleInside = nbPeopleInside;
    }
}