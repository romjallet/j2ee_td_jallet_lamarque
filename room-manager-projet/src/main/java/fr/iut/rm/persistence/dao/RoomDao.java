package fr.iut.rm.persistence.dao;

import fr.iut.rm.persistence.domain.Room;

import java.util.List;
import java.util.NoSuchElementException;

/**
 * Data Access Object of {@link fr.iut.rm.persistence.domain.Room}
 */
public interface RoomDao {

    /**
     * Persists room.
     *
     * @param room object to persist
     */
    void saveOrUpdate(Room room);

    /**
     * @return the full room list
     */
    List<Room> findAll();

    /**
     * @param name the searched name, must be case sensitive and exact match. Null name returns null result
     * @return the room or null if nothing found
     */
    Room findByName(final String name);

    /**
     * Try to remove the room with the given name
     *
     * @param name the name of the room you want to remove from the base
     */
    void remove(final String name) throws NoSuchElementException;
}