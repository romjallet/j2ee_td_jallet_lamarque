package fr.iut.rm.web.servlet;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import fr.iut.rm.persistence.dao.RoomDao;
import fr.iut.rm.persistence.domain.Room;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Servlet to call in order to get in or get out a room
 */
@Singleton
public class InOutServlet extends HttpServlet {
    /**
     * the dao to access rooms stored in DB *
     */
    @Inject
    RoomDao roomDao;

    /**
     * GET access
     * @param req nothing specified
     * @param resp nothing specified, must be HTML format
     * @throws javax.servlet.ServletException by container
     * @throws java.io.IOException by container
     */
    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
        // setting mime type
        resp.setContentType("text/html; charset=UTF-8") ;
        resp.setCharacterEncoding("UTF-8");

        //getting template
        Template freemarkerTemplate = null ;
        freemarker.template.Configuration freemarkerConfiguration = new freemarker.template.Configuration();
        freemarkerConfiguration.setClassForTemplateLoading(this.getClass(), "/") ;
        freemarkerConfiguration.setObjectWrapper(new DefaultObjectWrapper()) ;
        Map<String, Object> root = new HashMap<String, Object>() ;

        //getting parameters
        String name=req.getParameter("name");
        String url=req.getParameter("url");
        String type=req.getParameter("t");
        boolean error =false;
        //setting the return url (to root if not specified)
        if(url==null){
            root.put("url", req.getContextPath());
        }else {
            root.put("url", url);
        }
        root.put("root", req.getContextPath()+"/");

        //if no name specified, print an error
        if( name==null) {
            error=true;
            root.put("title", "Erreur, nom non spécifié !");
            root.put("message", "Si vous avez construit l'URL vous même, ou flashé un QRCode, il se peut qu'il y ait eu un problème.");
        }else {
            //if room can't be find, print an error
            Room room = roomDao.findByName(name);
            if (room == null) {

                error = true;
                root.put("title", "Erreur, salle spécifiée invalide !");
                root.put("message", "La salle à laquelle vous souhaitez accéder n'existe pas ou plus.");

            }else {

                if (type.equals("i")) {          // Go in
                    try {
                        room.setNbPeopleInside(room.getNbPeopleInside() + 1);
                        root.put("title", "Vous êtes bien rentrés dans la salle "+room.getName());
                    } catch (IllegalArgumentException e) {
                        error = true;
                        root.put("title", "Erreur, salle pleine !");
                        root.put("message", "La salle à laquelle vous souhaitez accéder est pleine; vous ne pouvez donc pas y entrer.");
                    }
                } else if (type.equals("o")) {    // Go out
                    try {
                        room.setNbPeopleInside(room.getNbPeopleInside() - 1);
                        root.put("title", "Vous êtes bien sortis dans la salle " + room.getName());
                    } catch (IllegalArgumentException e) {
                        error = true;
                        root.put("title", "Erreur, salle vide !");
                        root.put("message", "La salle à laquelle vous souhaitez accéder est vide; vous ne pouvez donc pas sortir... puisque vous ne pouvez pas être dedans.");
                    }
                } else {                  //error
                    error = true;
                    root.put("title", "Erreur, type d'accès à la salle invalide");
                    root.put("message", "Si vous avez construit l'URL vous même, ou flashé un QRCode, il se peut qu'il y ait eu un problème. En effet, '" + type + "' n'est pas un type d'accès valide.");
                }
                //IF we can get there, everything is fine
                roomDao.saveOrUpdate(room);
                root.put("isAdmin", false);
                root.put("room", room);
            }
        }
        if(error) {             //If there were an error, print a error template
            try {
                freemarkerTemplate = freemarkerConfiguration.getTemplate("templates/error.ftl");
            } catch (IOException e) {
                System.out.println("Impossible d'ouvrir le template demandé.");
            }
        }else{                  //If not, an info template
            try {
                freemarkerTemplate = freemarkerConfiguration.getTemplate("templates/roomDetail.ftl");
            } catch (IOException e) {
                System.out.println("Impossible d'ouvrir le template demandé.");
            }
        }
        // print out the template
        PrintWriter out = resp.getWriter() ;
        assert freemarkerTemplate != null ;
        try {
            freemarkerTemplate.process(root, out) ;
            out.close() ;
        } catch (TemplateException e) {
            e.printStackTrace() ;
        }
    }
}
