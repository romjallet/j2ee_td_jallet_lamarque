package fr.iut.rm.web.servlet;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import fr.iut.rm.persistence.dao.RoomDao;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * Servlet to add room (with form)
 */
@Singleton
public class AddRoomServlet extends HttpServlet {
    /**
     * the dao to access rooms stored in DB *
     */
    @Inject
    RoomDao roomDao;

    /**
     * GET access
     * @param req nothing specified
     * @param resp nothing specified, must be HTML format
     * @throws javax.servlet.ServletException by container
     * @throws java.io.IOException by container
     */
    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
        // setting mime type
        resp.setContentType("text/html; charset=UTF-8") ;
        resp.setCharacterEncoding("UTF-8");
        //getting template
        Template freemarkerTemplate = null ;
        freemarker.template.Configuration freemarkerConfiguration = new freemarker.template.Configuration();
        freemarkerConfiguration.setClassForTemplateLoading(this.getClass(), "/") ;
        freemarkerConfiguration.setObjectWrapper(new DefaultObjectWrapper()) ;
        try {
            freemarkerTemplate = freemarkerConfiguration.getTemplate("templates/AddRoom.ftl");
        } catch (IOException e) {
            System.out.println("Impossible d'ouvrir le template demandé.");
        }
        Map<String, Object> root = new HashMap<String, Object>() ;
        // navigation data and links
        root.put("title", "Ajouter une salle");
        root.put("root", req.getContextPath()+"/");

        PrintWriter out = resp.getWriter() ;
        assert freemarkerTemplate != null ;
        try {
            freemarkerTemplate.process(root, out) ;
            out.close() ;
        } catch (TemplateException e) {
            e.printStackTrace() ;
        }
    }
}
