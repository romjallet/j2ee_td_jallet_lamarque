package fr.iut.rm.web.servlet;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import fr.iut.rm.persistence.dao.RoomDao;
import fr.iut.rm.persistence.domain.Room;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import javax.persistence.PersistenceException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * This servlet simply
 */
@Singleton
public class InitServlet extends HttpServlet {
    /**
     * the dao used to access room persisted data
     */
    @Inject
    RoomDao roomDao;

    /**
     * HTTP GET access
     * @param req use an optional nb parameter to make evidence of transactionnal behavior volontary triggering an exception
     * @param resp response to sent
     * @throws ServletException by container
     * @throws IOException by container
     */
    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
        // setting mime type
        resp.setContentType("text/html; charset=UTF-8") ;
        resp.setCharacterEncoding("UTF-8");
        //preparing template (witch will be different according to some potential errors)
        Template freemarkerTemplate = null;
        freemarker.template.Configuration freemarkerConfiguration = new freemarker.template.Configuration();
        freemarkerConfiguration.setClassForTemplateLoading(this.getClass(), "/");
        freemarkerConfiguration.setObjectWrapper(new DefaultObjectWrapper());
        Map<String, Object> root = new HashMap<String, Object>();
        PrintWriter out = resp.getWriter();

        String name = req.getParameter("name");
        String desc = req.getParameter("description");
        String capacity=req.getParameter("capacity");
        root.put("root", req.getContextPath()+"/");
        root.put("url", req.getContextPath()+req.getServletPath());

        if (name == null || capacity==null) {
            try {
                freemarkerTemplate = freemarkerConfiguration.getTemplate("templates/error.ftl");
            } catch (IOException e) {
                System.out.println("Impossible d'ouvrir le template demandé.");
            }
            root.put("title", "Erreur: paramètres invalides !");
            root.put("message","Erreur: veuillez spécifier un paramètre 'name', 'capacity', et (éventuellement) 'description' !"+req.getServletPath()+" - "+req.getContextPath()+ " - "+req.getPathTranslated());
        }else{
            Room room1 = new Room();
            try {

                room1.setName("" + name);
                room1.setCapacity(Integer.parseInt(capacity));

                if (desc != null && desc != "") {
                    room1.setDescription(desc);
                } else {
                    room1.setDescription("Pas de description");
                }

                roomDao.saveOrUpdate(room1);

                //getting template
                try {
                    freemarkerTemplate = freemarkerConfiguration.getTemplate("templates/roomDetail.ftl");
                } catch (IOException e) {
                    System.out.println("Impossible d'ouvrir le template demandé.");
                }

                // navigation data and links
                root.put("room", room1);
                root.put("title", "Salle "+room1.getName()+" correctement créée.");
                root.put("isAdmin", true);

            }catch (NumberFormatException e){
                root.put("title", "Erreur: paramètres invalides !");
                root.put("message", "Erreur: '" + capacity + "' n'est pas un nombre !");
            }catch (IllegalArgumentException e){
                root.put("title", "Erreur: paramètres invalides !");
                root.put("message", "Erreur: " + e.getMessage());
            }catch(PersistenceException e){
                root.put("title", "Erreur: problème de génération de la salle !");
                root.put("message","Erreur lors de la création de la salle: Le nom donné est problablement déjà utilisé par une autre salle.");
            }finally {
                if(freemarkerTemplate==null){
                    try {
                        freemarkerTemplate = freemarkerConfiguration.getTemplate("templates/error.ftl");
                    } catch (IOException e) {
                        System.out.println("Impossible d'ouvrir le template demandé.");
                    }
                }
            }
        }// END else

        //print the template
        assert freemarkerTemplate != null;
        try {
            freemarkerTemplate.process(root, out);
            out.close();
        } catch (TemplateException e) {
            e.printStackTrace();
        }

    }
}